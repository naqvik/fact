/**
   Test code for factorial
 */

#include <iostream>
#include <cassert>
#include "factorial.h"          // provide factorial prototype

void testFact1is1() {
    assert(factorial(1) == 1);
}

int main() {
    testFact1is1();
}
