/**
   Return factorial(n)

   precondition: n >= 0
*/
int factorial(int n) {
    if (n == 0) return 1;
    return n * factorial(n-1);
}
